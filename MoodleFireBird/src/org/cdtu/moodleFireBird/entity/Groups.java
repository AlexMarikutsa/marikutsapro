package org.cdtu.moodleFireBird.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "GROUPS")
public class Groups {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotNull 
	@Column(name = "NAME")
	private String name;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "SPECIALITY_ID", insertable=false, updatable=false, nullable=false)
	@JsonBackReference
	private Speciality speciality;
	@Column(name = "ACTIVE1")
	private char active1;
	@Column(name = "TUTION_FORM")
	private String tutionForm;
	@Column(name = "CREATION_YEAR")
	private int creationYear;
	@Column(name = "KURS")
	private int kurs;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Speciality getSpeciality() {
		return speciality;
	}
	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}
	public char getActive1() {
		return active1;
	}
	public void setActive1(char active1) {
		this.active1 = active1;
	}
	public String getTutionForm() {
		return tutionForm;
	}
	public void setTutionForm(String tutionForm) {
		this.tutionForm = tutionForm;
	}
	public int getCreationYear() {
		return creationYear;
	}
	public void setCreationYear(int creationYear) {
		this.creationYear = creationYear;
	}
	public int getKurs() {
		return kurs;
	}
	public void setKurs(int kurs) {
		this.kurs = kurs;
	}
}
