package org.cdtu.moodleFireBird.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "SPECIALITIES")
public class Speciality {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotNull 
	@Column(name = "NAME")
	private String name;
	@Column(name = "SHIFR")
	private double shifr;
	@Column(name = "SHIFR_BACH")
	private double shifrBach;
	@Column(name = "QULIFICATIONS_SPEC")
	private String qualification;	
	@Column(name = "NAME_BACH")
	private String nameBach;
	@Column(name = "NAME_MASTER")
	private String nameMaster;
	@Column(name = "SHIFR_MASTER")
	private String shifrMaster;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getShifr() {
		return shifr;
	}
	public void setShifr(double shifr) {
		this.shifr = shifr;
	}
	public double getShifrBach() {
		return shifrBach;
	}
	public void setShifrBach(double shifrBach) {
		this.shifrBach = shifrBach;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getNameBach() {
		return nameBach;
	}
	public void setNameBach(String nameBach) {
		this.nameBach = nameBach;
	}
	public String getNameMaster() {
		return nameMaster;
	}
	public void setNameMaster(String nameMaster) {
		this.nameMaster = nameMaster;
	}
	public String getShifrMaster() {
		return shifrMaster;
	}
	public void setShifrMaster(String shifrMaster) {
		this.shifrMaster = shifrMaster;
	}
}
