package org.cdtu.moodleFireBird.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.cdtu.moodleFireBird.dao.SpringProjectDAO;
import org.cdtu.moodleFireBird.entity.CurrentYear;
import org.cdtu.moodleFireBird.entity.Groups;
import org.cdtu.moodleFireBird.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FirstController {	
	@Autowired
	SpringProjectDAO dao;

	@RequestMapping("/start.fire")
	public ModelAndView start(){
		ModelAndView mav=new ModelAndView("facultets");
		return mav;
	}
	@RequestMapping("/readSpec.fire")
	public @ResponseBody List<List<String>> subjects(HttpServletRequest request) {
		CurrentYear currentYear = dao.readCurrentYear();
		Subject subject = dao.oneSubject();
		List<Groups> groups = new ArrayList<Groups>();
		List<List<String>> subjects = new ArrayList<List<String>>();
		for(Groups g: subject.getGroups()){
			if(g.getActive1()== 'T'){
				groups.add(g);
			}
		}
		for(Groups g: groups){
			List<String> subjectString = new ArrayList<String>();
			subjectString.add(0,subject.getName());
			subjectString.add(0,(currentYear.getCurrentYear()-g.getCreationYear()+g.getKurs())+"-� ���� ��������");
			subjectString.add(0,g.getTutionForm()+" - ����� ��������");
			int kurs = currentYear.getCurrentYear()-g.getCreationYear()+g.getKurs();
			if(((kurs >= 1 && kurs <= 4) && g.getTutionForm().equals("�"))||
					((kurs >= 1 && kurs <= 5) && g.getTutionForm().equals("�"))) {
				subjectString.add(0,g.getSpeciality().getNameBach()+" "+g.getSpeciality().getShifrBach());			
			}
			else{
				if(((kurs == 5)&& g.getTutionForm().equals("�"))||
						((kurs == 6)&& g.getTutionForm().equals("�"))){
					if(g.getName().charAt(0) == '�'){
						subjectString.add(0,g.getSpeciality().getNameMaster()+" "+g.getSpeciality().getShifrMaster());									
					}else{
						subjectString.add(0,g.getSpeciality().getName()+" "+g.getSpeciality().getShifr());
					}
				}
			}
			if(subjects.isEmpty()){
				subjects.add(subjectString);				
			}else{
				boolean found = false;
				for(List<String> s: subjects){
					if(subjectString.equals(s)){
						found = true;
						break;
					}
				}
				if (!found)
					subjects.add(subjectString);
			}
		}
		return subjects;
	}
}
