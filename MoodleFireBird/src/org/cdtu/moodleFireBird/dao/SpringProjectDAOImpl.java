package org.cdtu.moodleFireBird.dao;

import org.cdtu.moodleFireBird.entity.CurrentYear;
import org.cdtu.moodleFireBird.entity.Subject;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SpringProjectDAOImpl implements SpringProjectDAO{

	@Autowired
	SessionFactory sessionFactory;

	@Transactional(readOnly=true)
	public Subject oneSubject() {
		return (Subject)sessionFactory.getCurrentSession().get(Subject.class,7);
	}
	@Transactional(readOnly=true)
	public CurrentYear readCurrentYear() {
		return (CurrentYear)sessionFactory.getCurrentSession().get(CurrentYear.class,1);
	}
}
