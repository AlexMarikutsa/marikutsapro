package org.cdtu.moodleFireBird.dao;

import org.cdtu.moodleFireBird.entity.CurrentYear;
import org.cdtu.moodleFireBird.entity.Subject;

public interface SpringProjectDAO {
	public Subject oneSubject();
	public CurrentYear readCurrentYear();
}
