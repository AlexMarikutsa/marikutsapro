<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>firebird database</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(document).ready(function(){
	    $.ajax({
	        url:"readSpec.fire",
	        type:"post",
	        data:'',
	        success:function(data){
				for(var i = 0; i<data.length; i++){
					var s = '';
					var ul = '';
					var element = data[i];
					for(var j = 0; j<element.length; j++){
						s += '<ul><li>'+element[j];	
						ul += '</li></ul>';
					}
					s += ul;				
					$('#database').append(s);
				}
	        }	       
	    });
	})
</script>
</head>
<body>
	<div id = "database">  </div>
</body>
</html>