package org.cdtu.dao;

import java.util.ArrayList;
import java.util.List;

import org.cdtu.entity.Alcohol;
import org.cdtu.entity.Provider;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SpringProjectDAOImpl implements SpringProjectDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional(readOnly=true)
	public List<Alcohol> readAlcohols() {
		return sessionFactory.getCurrentSession().createQuery("from Alcohol").list();
	}

	@Transactional(readOnly=true)
	public Alcohol readAlcohol(int alcId) {
		return (Alcohol) sessionFactory.getCurrentSession().load(Alcohol.class,alcId);
	}

	@Override
	@Transactional
	public void deleteAlcohol(int alcId) {
		Session session = sessionFactory.getCurrentSession();
		Alcohol alcohol = (Alcohol) session.load(Alcohol.class, alcId);
		session.delete(alcohol);
		session.flush();
	}

	@Override
	@Transactional
	public void editeAlcohol(int alcId, String alcName) {
		String alcoholName = alcName;
		Alcohol alcohol = (Alcohol) sessionFactory.getCurrentSession().load(Alcohol.class, alcId);
		alcohol.setName(alcoholName);
		sessionFactory.getCurrentSession().save(alcohol);
	}

	@Transactional(readOnly=true)
	public List<Provider> readProviders() {
		return sessionFactory.getCurrentSession().createQuery("from Provider").list();
	}
	
	@Override
	@Transactional
	public void createAlc(int providerId, String alcName) {
		String alcoholName = alcName;
		Alcohol alcohol = new Alcohol();
		alcohol.setName(alcoholName);
		List<Provider> providers = new ArrayList<Provider>();
		Provider provider = (Provider) sessionFactory.getCurrentSession().load(Provider.class, providerId);
		providers.add(provider);
		alcohol.setProviders(providers);
		sessionFactory.getCurrentSession().save(alcohol);
	}
}
