package org.cdtu.dao;

import java.util.List;

import org.cdtu.entity.Alcohol;
import org.cdtu.entity.Provider;

public interface SpringProjectDAO {
	public List<Alcohol> readAlcohols();
	public Alcohol readAlcohol(int alcId);
	public List<Provider> readProviders();
	public void deleteAlcohol(int alcId);
	public void editeAlcohol(int alcId, String alcName);
	public void createAlc(int providerId, String alcName);	
}
