package org.cdtu.controller;

import java.util.List;

import org.cdtu.dao.SpringProjectDAO;
import org.cdtu.entity.Alcohol;
import org.cdtu.entity.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FirstController {	
	@Autowired
	SpringProjectDAO dao;
	
	@RequestMapping("/start.do")
	public ModelAndView start(){
		List<Alcohol> alcohols = dao.readAlcohols();
		ModelAndView mav=new ModelAndView("start");
		mav.addObject("alcohols",alcohols);
		return mav;
	}
	@RequestMapping("/deleting.do")
	public ModelAndView deleting(@RequestParam Integer alcId){
		dao.deleteAlcohol(alcId);
		List<Alcohol> alcohols = dao.readAlcohols();
		ModelAndView mav=new ModelAndView("start");
		mav.addObject("alcohols",alcohols);
		return mav;
	}
	@RequestMapping("/editing.do")
	public ModelAndView editing(@RequestParam Integer alcId){
		ModelAndView mav=new ModelAndView("editing");
		Alcohol alc = dao.readAlcohol(alcId);
		mav.addObject("alc",alc);
		return mav;
	}
	@RequestMapping("/editAlc.do")
	public ModelAndView editAlc(@RequestParam Integer alcId, @RequestParam String alcName){
		dao.editeAlcohol(alcId, alcName);
		List<Alcohol> alcohols = dao.readAlcohols();
		ModelAndView mav=new ModelAndView("start");
		mav.addObject("alcohols",alcohols);
		return mav;
	}
	@RequestMapping("/creating.do")
	public ModelAndView creating(){
		List<Provider> providers = dao.readProviders();
		ModelAndView mav=new ModelAndView("createAlc");
		mav.addObject("providers",providers);
		return mav;
	}
	@RequestMapping("/createAlc.do")
	public ModelAndView createAlc(@RequestParam Integer providerId, @RequestParam String alcName){
		dao.createAlc(providerId, alcName);
		List<Alcohol> alcohols = dao.readAlcohols();
		ModelAndView mav=new ModelAndView("start");
		mav.addObject("alcohols",alcohols);
		return mav;
	}
	@RequestMapping("/site.do")
	public ModelAndView site(){
		List<Alcohol> alcohols = dao.readAlcohols();
		ModelAndView mav=new ModelAndView("tryCol");
		mav.addObject("alcohols",alcohols);
		return mav;
	}
}
