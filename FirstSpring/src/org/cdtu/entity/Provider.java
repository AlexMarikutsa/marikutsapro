package org.cdtu.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "provider")
public class Provider {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "name")
	private String name;
	@ManyToMany(mappedBy="providers")
	private List <Alcohol> alcohols;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Alcohol> getAlcohols() {
		return alcohols;
	}
	public void setAlcohols(List<Alcohol> alcohols) {
		this.alcohols = alcohols;
	}
	@Override
	public String toString(){
		return getName();
	}

}
