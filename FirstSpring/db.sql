-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.13 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-04-03 14:13:41
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for alcohol
CREATE DATABASE IF NOT EXISTS `alcohol` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `alcohol`;


-- Dumping structure for table alcohol.alcohol
CREATE TABLE IF NOT EXISTS `alcohol` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- Dumping data for table alcohol.alcohol: ~9 rows (approximately)
/*!40000 ALTER TABLE `alcohol` DISABLE KEYS */;
INSERT INTO `alcohol` (`id`, `name`, `price`) VALUES
	(25, 'Martiny&Wiscky', 143.2),
	(42, 'Brandy', 0),
	(45, 'Burn', 0),
	(46, 'Red Label', 0),
	(47, 'Prime', 0),
	(48, 'Пиво', 1),
	(49, 'Вино', 12),
	(50, 'віск', 198),
	(51, 'Capitan Morgan', 220);
/*!40000 ALTER TABLE `alcohol` ENABLE KEYS */;


-- Dumping structure for table alcohol.alcohol_providers
CREATE TABLE IF NOT EXISTS `alcohol_providers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `alcohols_id` int(10) NOT NULL,
  `providers_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- Dumping data for table alcohol.alcohol_providers: ~10 rows (approximately)
/*!40000 ALTER TABLE `alcohol_providers` DISABLE KEYS */;
INSERT INTO `alcohol_providers` (`id`, `alcohols_id`, `providers_id`) VALUES
	(19, 25, 3),
	(36, 42, 4),
	(39, 45, 2),
	(40, 46, 1),
	(41, 47, 13),
	(44, 48, 3),
	(45, 49, 13),
	(46, 49, 2),
	(47, 50, 13),
	(48, 51, 1);
/*!40000 ALTER TABLE `alcohol_providers` ENABLE KEYS */;


-- Dumping structure for table alcohol.provider
CREATE TABLE IF NOT EXISTS `provider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table alcohol.provider: ~5 rows (approximately)
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` (`id`, `name`) VALUES
	(1, 'ПОЛЯКОВ'),
	(2, 'МЕГАПОЛІС'),
	(3, 'БАЯДЕРА'),
	(4, 'БІЗНЕС-ГРАНД'),
	(13, 'Ходак');
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
