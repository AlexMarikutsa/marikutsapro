<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="try.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script	src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
	var t = $("#blockTop").css("background-color");
	$('#blockTop').mouseenter(function(){
		$("#blockTop").css("background-color", "yellow");
	}).mouseleave(function(){
		$("#blockTop").css("background-color", t);
	});
});
	$(document).ready(function(){
		$("ul").hide();
		$("h3").click(function(){
			$(this).next().slideToggle("slow").siblings("ul:visible").slideUp("slow");
		});
	});
	function changeColor(){
		$("#blockRight").css("background-color", "yellow");
	}
	function changePlace(){
		var position = $("#blockLeft").css("left");
		$("#blockLeft").css("left",$("#blockRight").css("left"));
		$("#blockRight").css("left",position);
	}
</script>
<title>JSP Columns</title>
</head>
<body>
	<div id="blockTop">ВЕРХНЯ КОЛОНКА This is a three columns liquid layout with the
		leftmost and rightmost columns being static, always in view.
		<input type = "button" value = "Змінити місцями" onclick = "changePlace();"/>
	</div>
	<div id="blockLeft">
		ЛІВА КОЛОНКА This is a three columns liquid layout with the leftmost and rightmost
		columns being static, always in view. 
		<input type="button" value="Змінити колір" onclick="changeColor();" />
	</div>
	<div id="blockRight">
		<h3>QQQQQQ</h3>
		<ul>
			<li>AAAAAAAAA</li>
			<li>BBBBBBBBB</li>
			<li>CCCCCCCCC</li>
		</ul>
		<h3>QQQQQQ</h3>
		<ul>
			<li>AAAAAAAAA</li>
			<li>BBBBBBBBB</li>
			<li>CCCCCCCCC</li>
		</ul>
		<h3>QQQQQQ</h3>
		<ul>
			<li>AAAAAAAAA</li>
			<li>BBBBBBBBB</li>
			<li>CCCCCCCCC</li>
		</ul>
	</div>
	<div id="blockMain"><%@include file="start.jsp"%></div>
</body>
</html>