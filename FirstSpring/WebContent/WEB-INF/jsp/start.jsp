<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script	src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
	var color;
	$('tr').on('mouseover', function() {
		color = $(this).css("background");
    	$(this).css('background', 'red');
	}).on('mouseout', function() {
    	$(this).css('background', color);
	});
});

function edit(alcId){
	$.get('editing.do',{alcId:alcId},function(data){
		$('#edit').html(data);
	});
}

function creating(){
	$.get('creating.do',function(data){
		$('#blockMain').html(data);
	})
}
function create2(){
	$('table').load('creating.do');
}
</script>
<link rel = "stylesheet" type = "text/css" href = "try.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Алкоголь та постачальники</title>
<style>
table,th,td {
	border: 1px solid black;
	border-collapse: collapse;
}
</style>
</head>
<body>

	<table style="width: 100%; position:relative; left: 0px; top: 0px;">
		<tr id = "nameCol">
			<th>Дія</th>
			<th>Алкоголь</th>
			<th>Постачальник</th>
		</tr>
		<c:forEach items="${alcohols}" var="n">
			<tr>
				<td>
					<form action='editing.do'>
						<input type="hidden" name="alcId" value="${n.id}" />
						<p>
							<input type="button" class="b1" value="Редагувати" onclick="edit(${n.id});">
						</p>
					</form>
					<form action='deleting.do' style="position:relative; left: 90px; top: -35px;">
						<input type="hidden" name="alcId" value="${n.id}" />
						<p>
							<input type="submit" class="b2" value="Видалити">
						</p>
					</form>

				</td>

				<td>${n.name}</td>
				<td><c:forEach items="${n.providers}" var="p">
						<p>${p.name}</p>
					</c:forEach></td>
		</c:forEach>
	</table>
	<a href="Javascript:creating()">Додати новий алкоголь</a>
	<div id="edit">
	</div>

</body>
</html>