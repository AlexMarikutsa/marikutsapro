<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script	src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script>
	function create(providerId,alcName){
		$.get('createAlc.do',{providerId:providerId,alcName:alcName},function(data){
			$('#blockMain').html(data);
		});
	}
</script>
</head>
<body>
<form action='start.do' method = 'post'>
	<p>Alcohol name</p>
	<input id ='alcName' type='text'  />
	<p>Provider name</p>
	<select size='1' id='providerId'>
	<c:forEach items="${providers}" var="s">
		<option value="${s.id}">"${s.name}"</option>
	</c:forEach>
	</select>
	<p><input type='button' value='add' onclick = 'create($("#providerId").val(),$("#alcName").val());'/></p>
</form>
</body>
</html>