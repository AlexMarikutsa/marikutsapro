<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>autocomplete demo</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="js/jstree.js"></script>
<link rel="stylesheet" href="js/themes/default/style.min.css" />
<script>
$(document).ready(function(){   
    $.ajax({
     url:"autocompleteCourseNames.cdtu",
     type:"post",
     data:'',
     success:function(data){
      $("#autocomplete").autocomplete({   
          source: data,
        });
     },error:  function(data, status, er){
              console.log(data+"_"+status+"_"+er);
          },
    });
  });
  function makeTree(courseName){
	  $.ajax({
		  url: "choseCourse.cdtu",
		  type: "POST",
		  data: {courseName:courseName},
		  success: function(data) {
			for(var i = 0; i<data.length; i++){
				$('#tree').append('<p>'+data[i].fullname+'</p>'+
					    '<p>'+data[i].category.name+'</p>');
			}
		  }
	  });
  } 
  function makeTree2(courseName){
	  $.ajax({
		  url: "choseCourseAndCategory.cdtu",
		  type: "POST",
		  data: {courseName:courseName},
		  success: function(data) {
			for(var i = 0; i<data.length; i++){
				var s = '';
				var ul = '';
				var element = data[i];
				for(var j = 0; j<element.length; j++){
					s += '<ul><li>'+element[j];	
					ul += '</li></ul>';
				}
				s += ul;				
				$('#tree').append(s);
			}
		  }
	  });
  }
	function makeTreeStudentsNames(courseName){
	  $.ajax({
		  url: "courseStudets.cdtu",
		  type: "POST",
		  data: {courseName:courseName},
		  success: function(data) {
			for(var i = 0; i<data.length; i++){
				var s = '';
				var element = data[i];
				var c = element[0];
				var ul = '<ul><li>'+ c + '<ul><li>';
				for(var j = 1; j<element.length; j++){
					s +=element[j]+'<p>';	
				}
				s = ul + s + '</li></ul></li></ul>';				
				$('#tree').append(s);
			}
		  }
	  });
	} 
</script>
</head>
<body>
	<form action="">
		<label for="autocomplete">Виберіть категорію : </label>
		<input id="autocomplete" type='text'>
		<input type='button' value='показати категорії' onclick = 'makeTree2($("#autocomplete").val())'/>
		<input type='button' value='показати студентів' onclick = 'makeTreeStudentsNames($("#autocomplete").val())'/>
	</form>
	<div id = "tree">	</div>
	<script>		
		$(function() {
			$('#tree').jstree();
			$('#tree').on("changed.jstree", function(e, data) {
				console.log(data.selected);
			});
		});
	</script>
</body>
</html>