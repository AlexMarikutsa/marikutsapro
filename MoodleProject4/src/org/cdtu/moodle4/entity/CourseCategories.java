package org.cdtu.moodle4.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "mdl_course_categories")
public class CourseCategories {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "category",fetch=FetchType.EAGER)
	@JsonManagedReference
	private List<Course> courses;
	@NotNull 
	private String name;
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name = "parent")
	@JsonManagedReference
	private Set<CourseCategories> children = new HashSet<CourseCategories>();
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "parent",insertable=false,updatable=false)
	@JsonBackReference
	private CourseCategories parent;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public CourseCategories getParent() {
//		return parent;
//	}
//	public void setParent(CourseCategories parent) {
//		this.parent = parent;
//	}
//	public List<CourseCategories> getChild() {
//		return child;
//	}
//	public void setChild(List<CourseCategories> child) {
//		this.child = child;
//	}

	public Set<CourseCategories> getChildren() {
		return children;
	}
	public void setChildren(Set<CourseCategories> children) {
		this.children = children;
	}
	public CourseCategories getParent() {
		return parent;
	}
	public void setParent(CourseCategories parent) {
		this.parent = parent;
	}
	public List<Course> getCourses() {
		return courses;
	}
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
}
