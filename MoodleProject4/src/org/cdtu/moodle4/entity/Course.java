package org.cdtu.moodle4.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "mdl_course")
public class Course {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "category", insertable=false, updatable=false, nullable=false)
	@JsonBackReference
	private CourseCategories category;
	@Column(name = "fullname")
	private String fullname;
	private String shortname;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "course",fetch=FetchType.EAGER)
	@JsonManagedReference
	private List<Enrol> enrols;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public List<Enrol> getEnrols() {
		return enrols;
	}
	public void setEnrols(List<Enrol> enrols) {
		this.enrols = enrols;
	}
	public CourseCategories getCategory() {
		return category;
	}
	public void setCategory(CourseCategories category) {
		this.category = category;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
}
