package org.cdtu.moodle4.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.cdtu.moodle4.dao.SpringProjectDAO;
import org.cdtu.moodle4.entity.Course;
import org.cdtu.moodle4.entity.CourseCategories;
import org.cdtu.moodle4.entity.Enrol;
import org.cdtu.moodle4.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FirstController {	
	@Autowired
	SpringProjectDAO dao;

//	@RequestMapping("/start.cdtu")
//	public ModelAndView start(){
//		ModelAndView mav=new ModelAndView("start");
//		return mav;
//	}
//	
//	@RequestMapping("/cc.cdtu")
//	public ModelAndView cc(){
//		CourseCategories root = dao.readRoot();
//		ModelAndView mav=new ModelAndView("jsTree");
//		mav.addObject("root",root);
//		return mav;
//	} 
//	
//	@RequestMapping(value = "/jsonTable.cdtu")
//	public void jsonTable(HttpServletRequest request, HttpServletResponse response) throws IOException{
//		response.setContentType("application/json");
//		Course course = dao.readCourse();
//		JSONObject courseJson = new JSONObject(course);
//		
//		List<Course> courses = dao.readCourses();
//		JSONArray coursesJSON = new JSONArray(courses);
//		
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().write(coursesJSON.toString());
//	}
//	
//	@RequestMapping(value = "/jsonCourseName.cdtu")
//	public void jsonCourseName(HttpServletRequest request, HttpServletResponse response) throws IOException{
//		response.setContentType("application/json");
//		CourseCategories courseCategory = dao.readCourseCategory();
//		JSONObject courseCategoryJson = new JSONObject(courseCategory);		
//		response.setCharacterEncoding("UTF-8");
//		response.getWriter().write(courseCategoryJson.toString());
//	}
//	@RequestMapping("/coursesJackson.cdtu")
//	public @ResponseBody List<Course> analysis(HttpServletRequest request) {			
//		List<Course> courses = dao.readCourses();
//		return courses;
//	}

//	@RequestMapping("/courseCategoriesJackson.cdtu")
//	public @ResponseBody List<CourseCategories> analyse(HttpServletRequest request) {			
//		List<CourseCategories> courseCategories = dao.readCategory();
//		return courseCategories;
//	}
//	class Auto{
//		private String value;
//		private String data;
//		
//		public String getValue() {
//			return value;
//		}
//		public void setValue(String value) {
//			this.value = value;
//		}
//		public String getData() {
//			return data;
//		}
//		public void setData(String data) {
//			this.data = data;
//		}
//		
//	}
//	@RequestMapping("/autocomplete.cdtu")
//	public @ResponseBody List<Auto> courseCategories(HttpServletRequest request) {			
//		List<CourseCategories> courseCategories = dao.readCategory();
//		List<Auto> autos = new ArrayList<Auto>();
//		for (CourseCategories cc: courseCategories){
//			Auto auto = new Auto();
//			auto.setValue(cc.getName());
//			auto.setData(cc.getName());
//			autos.add(auto);
//		}
//		return autos;
//	}
//	@RequestMapping("/start2.cdtu")
//	public ModelAndView start2(){
//		ModelAndView mav=new ModelAndView("jQueryUIAutocomplete");
//		List<Course> courses = dao.readCourses();
//		mav.addObject("courses", courses);
//		return mav;
//	}
//	@RequestMapping("/autocompleteNames.cdtu")
//	public @ResponseBody List<CourseCategories> courseCategoriesNames(HttpServletRequest request) {			
//		List<CourseCategories> courseCategories = dao.readCategoryNames();
//		return courseCategories;
//	}

	@RequestMapping("/start3.cdtu")
	public ModelAndView start3(){
		ModelAndView mav=new ModelAndView("jQueryUIAutocomplete");
		List<Course> courses = dao.readCoursesAll();
		mav.addObject("courses", courses);
		return mav;
	}
	@RequestMapping("/autocompleteCourseNames.cdtu")
	public @ResponseBody List<String> coursesNames(HttpServletRequest request) {			
		List<Course> courses = dao.readCoursesAll();
		List<String> coursesNames = new ArrayList<String>();
		for (Course c: courses){
			coursesNames.add(c.getFullname());
		}
		return coursesNames;
	}
	@RequestMapping("/choseCourse.cdtu")
	public @ResponseBody List<Course> courses(@RequestParam String courseName, HttpServletRequest request) {			
		List<Course> courses = dao.readCoursesAll();
		List<Course> choseCourse = new ArrayList<>();
		for(Course c: courses){
			if(courseName.equals(c.getFullname()) ){
				choseCourse.add(c);
			}
		}
		return choseCourse;
	}
	@RequestMapping("/choseCourseAndCategory.cdtu")
	public @ResponseBody List<List<String>> names(@RequestParam String courseName, HttpServletRequest request) {			
		List<Course> courses = dao.readCoursesAll();
		List<List<String>> names = new ArrayList<List<String>>();
		List<Course> choseCourse = new ArrayList<>();
		for(Course c: courses){
			if(courseName.equals(c.getFullname())){
				choseCourse.add(c);
			}
		}
		for(Course c: choseCourse){
			List<String> inner = new ArrayList<String>();
			inner.add(c.getFullname());
			names.add(inner);
			CourseCategories cc = c.getCategory();
			while(cc.getParent() != null){
				cc = cc.getParent();
				inner.add(0, cc.getName());
			}
		}
		return names;
	}

	@RequestMapping("/courseStudets.cdtu")
	public @ResponseBody List<List<String>> namesStudents(@RequestParam String courseName, HttpServletRequest request) {			
		List<Course> courses = dao.readCoursesAll();
		List<List<String>> namesStudents = new ArrayList<List<String>>();
		List<Course> choseCourse = new ArrayList<>();
		for(Course c: courses){
			if(courseName.equals(c.getFullname())){
				choseCourse.add(c);
			}
		}
		for(Course c: choseCourse){
			List<String> inner = new ArrayList<String>();
			inner.add(c.getFullname());
			for(Enrol e: c.getEnrols()){
				for(User u: e.getUsers()){
					inner.add(u.getLastname()+" "+u.getFirstname());
				}				
			}
			namesStudents.add(inner);
		}
		return namesStudents;
	}
}
