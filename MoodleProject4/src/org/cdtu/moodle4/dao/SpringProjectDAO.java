package org.cdtu.moodle4.dao;

import java.util.List;

import org.cdtu.moodle4.entity.Course;
import org.cdtu.moodle4.entity.CourseCategories;

public interface SpringProjectDAO {
	public Course readCourse();
	public List<Course> readCourses();
	public List<CourseCategories> readCategory();
	public CourseCategories readRoot();
	public CourseCategories readCourseCategory();
	public List<CourseCategories> readCategoryNames();
	public List<Course> readCoursesAll();
}
