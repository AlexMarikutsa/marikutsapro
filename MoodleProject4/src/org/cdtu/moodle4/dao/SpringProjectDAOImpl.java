package org.cdtu.moodle4.dao;

import java.util.List;

import org.cdtu.moodle4.entity.Course;
import org.cdtu.moodle4.entity.CourseCategories;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SpringProjectDAOImpl implements SpringProjectDAO{

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional(readOnly=true)
	public Course readCourse() {
		return (Course)sessionFactory.getCurrentSession().get(Course.class,397);
	}

	@Transactional(readOnly=true)
	public List<CourseCategories> readCategory() {
		return sessionFactory.getCurrentSession().createQuery("from CourseCategories").list();
	}
	
	public CourseCategories readRoot(){
		return (CourseCategories)sessionFactory.getCurrentSession().get(CourseCategories.class,2);
	}
	@Transactional(readOnly=true)
	public List<Course> readCourses() {
		return sessionFactory.getCurrentSession().createQuery("from Course").setMaxResults(10).list();
	}

	public CourseCategories readCourseCategory(){
		return (CourseCategories)sessionFactory.getCurrentSession().get(CourseCategories.class,11);
	}
	@Transactional(readOnly=true)
	public List<CourseCategories> readCategoryNames() {
		return sessionFactory.getCurrentSession().createQuery("select name from Course").list();
	}
	@Transactional(readOnly=true)
	public List<Course> readCoursesAll() {
		return sessionFactory.getCurrentSession().createQuery("from Course").list();
	}
}
